package io.mpac.vendorandcontacts.accounts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.mpac.vendorandcontacts.contacts.Contact;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;

	public List<Account> getAllAccounts() {

		List<Account> accounts = new ArrayList<>();
		accountRepository.findAll().forEach(accounts::add);

		return accounts;

	}

	public Account getAccountById(String id) {

		return accountRepository.findById(id).get();
	}

	public List<Contact> getAllContactsByAccount(String id) {

		List<Contact> contacts = null;
		if (accountRepository.findById(id).isPresent()) {
			contacts = accountRepository.findById(id).get().getContacts();
		}
		;

		return contacts;
	}

	public String addAccountInRepo(Account account) {
		
		if (accountRepository.save(account) != null) {
			return "Account Successfully added";
		} else {
			return "Account addition failed";
		}

	}

	public String updateAccountInRepo(Account account) {

		if (accountRepository.save(account) != null) {
			return "Account Successfully updated";
		} else {
			return "Account update failed";
		}
	}
}
