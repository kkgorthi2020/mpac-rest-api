package io.mpac.vendorandcontacts.accounts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.mpac.vendorandcontacts.contacts.Contact;

@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	// Get method for retrieving all the vendor accounts
	@RequestMapping("/mpac/v1/accounts")
	public List<Account> getAllAccounts() {

		return accountService.getAllAccounts();

	}

	// Get method for retrieving a specific vendor account
	@RequestMapping("/mpac/v1/accounts/{id}")
	public Account getAccountById(@PathVariable String id) {

		return accountService.getAccountById(id);

	}

	// Get method for retrieving all the contacts for a vendor account
	@RequestMapping("/mpac/v1/accounts/{id}/contacts")
	public List<Contact> getAllContactsByAccount(@PathVariable String id) {

		return accountService.getAllContactsByAccount(id);

	}

	// POST method for creating a new vendor account
	@RequestMapping(method = RequestMethod.POST, value = "/mpac/v1/addAccount")
	public String addAccount(@RequestBody Account account) throws JsonProcessingException {
	
		return accountService.addAccountInRepo(account);
	}

	// PUT method for updating an existing vendor account
	@RequestMapping(method = RequestMethod.PUT, value = "/mpac/v1/updateAccount")
	public String updateAccount(@RequestBody Account account) {

		return accountService.updateAccountInRepo(account);
	}

}