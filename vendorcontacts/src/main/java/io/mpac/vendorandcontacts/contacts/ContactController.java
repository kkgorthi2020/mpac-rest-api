package io.mpac.vendorandcontacts.contacts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContactController {

	@Autowired
	private ContactService contactService;

	// Get method for retrieving all the contacts
	@RequestMapping("/mpac/v1/contacts")
	public List<Contact> getAllContacts() {

		return contactService.getAllContacts();

	}

	// Get method for retrieving a specific contact
	@RequestMapping("/mpac/v1/contacts/{id}")
	public Contact getContactById(@PathVariable String id) {

		return contactService.getContactById(id);

	}

	// POST method for creating a new contact
	@RequestMapping(method = RequestMethod.POST, value = "/mpac/v1/addContact")
	public String addContact(@RequestBody Contact contact) {

		return contactService.addContactInRepo(contact);
	}

	// PUT method for updating an existing contact
	@RequestMapping(method = RequestMethod.PUT, value = "/mpac/v1/updateContact")
	public String updateContact(@RequestBody Contact contact) {

		return contactService.updateContactInRepo(contact);
	}

}