package io.mpac.vendorandcontacts.contacts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactService {

	@Autowired
	private ContactRepository contactRepository;

	public List<Contact> getAllContacts() {

		List<Contact> contacts = new ArrayList<>();
		contactRepository.findAll().forEach(contacts::add);
		return contacts;

	}

	public Contact getContactById(String id) {

		return contactRepository.findById(id).get();
	}

	public String addContactInRepo(Contact contact) {

		if (contactRepository.save(contact) != null) {
			return "Contact Successfully added";
		} else {
			return "Contact update failed";
		}
	}

	public String updateContactInRepo(Contact contact) {

		if (contactRepository.save(contact) != null) {
			return "Contact Successfully updated";
		} else {
			return "Contact update failed";
		}
	}
}
