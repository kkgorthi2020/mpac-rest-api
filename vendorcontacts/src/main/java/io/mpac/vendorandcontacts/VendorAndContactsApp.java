package io.mpac.vendorandcontacts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VendorAndContactsApp {

	public static void main(String[] args) {

		SpringApplication.run(VendorAndContactsApp.class, args);

	}

}