package io.mpac.vendorandcontacts.accounts;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.mpac.vendorandcontacts.VendorAndContactsApp;
import io.mpac.vendorandcontacts.contacts.Contact;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = VendorAndContactsApp.class)
@AutoConfigureMockMvc
class AccountControllerTests {

	@Autowired
	private MockMvc mockmvc;

	@MockBean
	private AccountService accountService;

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void getAllAccountsTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		List<Account> accountList = new ArrayList<>();

		Account account = new Account();
		account.setId("123");
		account.setCompanyName("PANW");
		account.setAddressLine1("add line 11");
		account.setAddressLine2("add line 12");
		account.setCity("Santa Clara");
		account.setState("California");
		account.setPostalCode("95051");
		account.setCountry("USA");

		Account account1 = new Account();
		account1.setId("234");
		account1.setCompanyName("PAN");
		account1.setAddressLine1("add line 11");
		account1.setAddressLine2("add line 12");
		account1.setCity("Santa Clara");
		account1.setState("California");
		account1.setPostalCode("95051");
		account1.setCountry("USA");

		accountList.add(account);
		accountList.add(account1);

		Mockito.when(accountService.getAllAccounts()).thenReturn(accountList);

		RequestBuilder request = MockMvcRequestBuilders.get("/mpac/v1/accounts")
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult mvcResult = mockmvc.perform(request).andReturn();

		assertEquals(mapper.writeValueAsString(accountList), mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void getAccountByIdTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		Optional<Account> repoAccount;

		Account account = new Account();
		account.setId("123");
		account.setCompanyName("PANW");
		account.setAddressLine1("add line 11");
		account.setAddressLine2("add line 12");
		account.setCity("Santa Clara");
		account.setState("California");
		account.setPostalCode("95051");
		account.setCountry("USA");

		repoAccount = Optional.ofNullable(account);

		Mockito.when(accountService.getAccountById(account.getId())).thenReturn(account);

		RequestBuilder request = MockMvcRequestBuilders.get("/mpac/v1/accounts/" + account.getId() + "")
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult mvcResult = mockmvc.perform(request).andReturn();

		assertEquals(mapper.writeValueAsString(repoAccount.get()), mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void addAccountTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		List<Contact> contactList = new ArrayList<Contact>();

		Contact contact = new Contact();
		contact.setId("444");
		contact.setName("my name");
		contact.setEmailAddress("second@gmail.com");
		contact.setAddressLine1("add line 1");
		contact.setAddressLine2("add line 2");
		contact.setCity("Irving");
		contact.setState("Texas");
		contact.setPostalCode("75051");
		contact.setCountry("USA");

		contactList.add(contact);

		Contact contact1 = new Contact();
		contact1.setId("222");
		contact1.setName("your name");
		contact1.setEmailAddress("first@gmail.com");
		contact1.setAddressLine1("add line 11");
		contact1.setAddressLine2("add line 21");
		contact1.setCity("San Jose");
		contact1.setState("California");
		contact1.setPostalCode("95231");
		contact1.setCountry("USA");

		contactList.add(contact1);

		Account account = new Account();
		account.setId("123");
		account.setCompanyName("PANW");
		account.setAddressLine1("add line 11");
		account.setAddressLine2("add line 12");
		account.setCity("Santa Clara");
		account.setState("California");
		account.setPostalCode("95051");
		account.setCountry("USA");
		account.setContacts(contactList);

		String successResponse = "Account Successfully added";

		Mockito.when(accountService.addAccountInRepo(Mockito.any(Account.class))).thenReturn(successResponse);

		RequestBuilder request = MockMvcRequestBuilders.post("/mpac/v1/addAccount")
				.contentType(MediaType.APPLICATION_JSON).header("method", "POST")
				.content(mapper.writeValueAsString(account));

		MvcResult mvcResult = mockmvc.perform(request).andReturn();

		assertEquals(successResponse, mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void udpateAccountTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		List<Contact> contactList = new ArrayList<Contact>();

		Contact contact = new Contact();
		contact.setId("444");
		contact.setName("my name");
		contact.setEmailAddress("second@gmail.com");
		contact.setAddressLine1("add line 1");
		contact.setAddressLine2("add line 2");
		contact.setCity("Irving");
		contact.setState("Texas");
		contact.setPostalCode("75051");
		contact.setCountry("USA");

		contactList.add(contact);

		Contact contact1 = new Contact();
		contact1.setId("222");
		contact1.setName("your name");
		contact1.setEmailAddress("first@gmail.com");
		contact1.setAddressLine1("add line 11");
		contact1.setAddressLine2("add line 21");
		contact1.setCity("San Jose");
		contact1.setState("California");
		contact1.setPostalCode("95231");
		contact1.setCountry("USA");

		contactList.add(contact1);

		Account account = new Account();
		account.setId("123");
		account.setCompanyName("PANW");
		account.setAddressLine1("add line 11");
		account.setAddressLine2("add line 12");
		account.setCity("Santa Clara");
		account.setState("California");
		account.setPostalCode("95051");
		account.setCountry("USA");
		account.setContacts(contactList);

		String successResponse = "Account Successfully updated";

		Mockito.when(accountService.updateAccountInRepo(Mockito.any(Account.class))).thenReturn(successResponse);

		RequestBuilder request = MockMvcRequestBuilders.put("/mpac/v1/updateAccount")
				.contentType(MediaType.APPLICATION_JSON).header("method", "PUT")
				.content(mapper.writeValueAsString(account));
		MvcResult mvcResult = mockmvc.perform(request).andReturn();

		assertEquals(successResponse, mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void getAllContactsByAccountTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		List<Contact> contactList = new ArrayList<Contact>();

		Contact contact = new Contact();
		contact.setId("444");
		contact.setName("my name");
		contact.setEmailAddress("second@gmail.com");
		contact.setAddressLine1("add line 1");
		contact.setAddressLine2("add line 2");
		contact.setCity("Irving");
		contact.setState("Texas");
		contact.setPostalCode("75051");
		contact.setCountry("USA");

		contactList.add(contact);

		Contact contact1 = new Contact();
		contact1.setId("222");
		contact1.setName("your name");
		contact1.setEmailAddress("first@gmail.com");
		contact1.setAddressLine1("add line 11");
		contact1.setAddressLine2("add line 21");
		contact1.setCity("San Jose");
		contact1.setState("California");
		contact1.setPostalCode("95231");
		contact1.setCountry("USA");

		contactList.add(contact1);

		Account account = new Account();
		account.setId("123");
		account.setCompanyName("PANW");
		account.setAddressLine1("add line 11");
		account.setAddressLine2("add line 12");
		account.setCity("Santa Clara");
		account.setState("California");
		account.setPostalCode("95051");
		account.setCountry("USA");
		account.setContacts(contactList);

		Mockito.when(accountService.getAllContactsByAccount(account.getId())).thenReturn(contactList);

		RequestBuilder request = MockMvcRequestBuilders.get("/mpac/v1/accounts/" + account.getId() + "/contacts")
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult mvcResult = mockmvc.perform(request).andReturn();

		assertEquals(mapper.writeValueAsString(contactList), mvcResult.getResponse().getContentAsString());

	}

}