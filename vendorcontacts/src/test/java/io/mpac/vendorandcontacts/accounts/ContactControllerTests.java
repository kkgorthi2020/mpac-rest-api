package io.mpac.vendorandcontacts.accounts;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.mpac.vendorandcontacts.VendorAndContactsApp;
import io.mpac.vendorandcontacts.contacts.Contact;
import io.mpac.vendorandcontacts.contacts.ContactService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = VendorAndContactsApp.class)
@AutoConfigureMockMvc
class ContactControllerTests {

	@Autowired
	private MockMvc mockmvc;

	@MockBean
	private ContactService contactService;

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void getAllContactsTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		List<Contact> contactList = new ArrayList<>();

		Contact contact = new Contact();
		contact.setId("444");
		contact.setName("my name");
		contact.setEmailAddress("second@gmail.com");
		contact.setAddressLine1("add line 1");
		contact.setAddressLine2("add line 2");
		contact.setCity("Irving");
		contact.setState("Texas");
		contact.setPostalCode("75051");
		contact.setCountry("USA");

		Contact contact1 = new Contact();
		contact1.setId("222");
		contact1.setName("your name");
		contact1.setEmailAddress("first@gmail.com");
		contact1.setAddressLine1("add line 11");
		contact1.setAddressLine2("add line 21");
		contact1.setCity("San Jose");
		contact1.setState("California");
		contact1.setPostalCode("95231");
		contact1.setCountry("USA");

		contactList.add(contact);
		contactList.add(contact1);

		Mockito.when(contactService.getAllContacts()).thenReturn(contactList);

		RequestBuilder request = MockMvcRequestBuilders.get("/mpac/v1/contacts")
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult mvcResult = mockmvc.perform(request).andReturn();

		assertEquals(mapper.writeValueAsString(contactList), mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void getContactById() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		Optional<Contact> repoContact;

		Contact contact = new Contact();
		contact.setId("444");
		contact.setName("my name");
		contact.setEmailAddress("second@gmail.com");
		contact.setAddressLine1("add line 1");
		contact.setAddressLine2("add line 2");
		contact.setCity("Irving");
		contact.setState("Texas");
		contact.setPostalCode("75051");
		contact.setCountry("USA");

		repoContact = Optional.ofNullable(contact);

		Mockito.when(contactService.getContactById(contact.getId())).thenReturn(contact);

		RequestBuilder request = MockMvcRequestBuilders.get("/mpac/v1/contacts/" + contact.getId() + "")
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult mvcResult = mockmvc.perform(request).andReturn();

		assertEquals(mapper.writeValueAsString(repoContact.get()), mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void addContactTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		Contact contact = new Contact();
		contact.setId("444");
		contact.setName("my name");
		contact.setEmailAddress("second@gmail.com");
		contact.setAddressLine1("add line 1");
		contact.setAddressLine2("add line 2");
		contact.setCity("Irving");
		contact.setState("Texas");
		contact.setPostalCode("75051");
		contact.setCountry("USA");

		String successResponse = "Contact Successfully added";

		Mockito.when(contactService.addContactInRepo(Mockito.any(Contact.class))).thenReturn(successResponse);

		RequestBuilder request = MockMvcRequestBuilders.post("/mpac/v1/addContact")
				.contentType(MediaType.APPLICATION_JSON).header("method", "POST")
				.content(mapper.writeValueAsString(contact));

		MvcResult mvcResult = mockmvc.perform(request).andReturn();

		assertEquals(successResponse, mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void updateContactTest() throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		Contact contact = new Contact();
		contact.setId("444");
		contact.setName("my name");
		contact.setEmailAddress("second@gmail.com");
		contact.setAddressLine1("add line 1");
		contact.setAddressLine2("add line 2");
		contact.setCity("Irving");
		contact.setState("Texas");
		contact.setPostalCode("75051");
		contact.setCountry("USA");

		String successResponse = "Contact Successfully updated";

		Mockito.when(contactService.updateContactInRepo(Mockito.any(Contact.class))).thenReturn(successResponse);

		RequestBuilder request = MockMvcRequestBuilders.put("/mpac/v1/updateContact")
				.contentType(MediaType.APPLICATION_JSON).header("method", "PUT")
				.content(mapper.writeValueAsString(contact));
		MvcResult mvcResult = mockmvc.perform(request).andReturn();

		assertEquals(successResponse, mvcResult.getResponse().getContentAsString());

	}

}