# REST API Vendor and Contact application for MPAC

This code contains REST API to solve MPAC problem of adding vendor accounts and contact.

The API solves the below use cases

Acount endpoint handle GET/POST/PUT requests.
Contact endpoint handle GET/POST/PUT requests.
Associates a contact with an account
Get all contacts for an account


#Tech Stack
Java

Spring Boot - REST framework

Apache Tomact - Server

JPA with Apache Derby - Database

Maven - Build tool


## Prerequisites

Java - 1.8

Maven - 3.6.0


## Install

    Download Spring Tool Suite 4 from the below link. For Mac OS choose the "MacOS - 64bit"
	https://spring.io/tools
	
	Git clone the code base and open in the Spring Tool Suite 4 as Maven project.
	
	Run "mvn clean install" on the root path to install all maven dependecies.
	

## Run the app

  	Run as "Java Application" in Spring Tool Suite 4
	
	Rest API runs on localhost:8080 by default

## Test the app

    Run "mvn test" to run all the test cases
    
## Suggested milestones
Can the server be started using the documentation?

    Yes. 
    Once the code is cloned and deployed into local machine and "mvn clean install" is run to get all maven dependencies,
    Open the application in STS4 and click Run the "VendorAndContactsApp.java" as "Java Application".
    This will start the server on port localhost:8080
    
    
Does the application connect to a database?

    Yes.
    The application is connected to inbuilt springboot database Derby.
    
    
Does the account endpoint handle GET/POST/PUT requests?

    Yes.
    GET -
        get all accounts, URL - http://localhost:8080/mpac/v1/accounts
        get account by Id - http://localhost:8080/mpac/v1/accounts/{id} [id - account Id]
    POST - 
        add account, URL - http://localhost:8080/mpac/v1/addAccount
                    Request body - id, companyName, addressLine1, addressLine2, city, state, postalCode, country, contacts.
        Sample Json object - {"id":"123","companyName":"PANW","addressLine1":"add line 11","addressLine2":"add line 12","city":"Santa Clara","state":"California","postalCode":"95051","country":"USA","contacts":[{"id":"444","name":"my name","emailAddress":"second@gmail.com","addressLine1":"add line 1","addressLine2":"add line 2","city":"Irving","state":"Texas","postalCode":"75051","country":"USA"},{"id":"222","name":"your name","emailAddress":"first@gmail.com","addressLine1":"add line 11","addressLine2":"add line 21","city":"San Jose","state":"California","postalCode":"95231","country":"USA"}]}
    PUT - 
        update account, URL - http://localhost:8080/mpac/v1/updateAccount
                        Request body - id, companyName, addressLine1, addressLine2, city, state, postalCode, country, contacts.
        Sample Json object - {"id":"123","companyName":"PANW","addressLine1":"add line 11","addressLine2":"add line 12","city":"Santa Clara","state":"California","postalCode":"95051","country":"USA","contacts":[{"id":"444","name":"my name","emailAddress":"second@gmail.com","addressLine1":"add line 1","addressLine2":"add line 2","city":"Irving","state":"Texas","postalCode":"75051","country":"USA"},{"id":"222","name":"your name","emailAddress":"first@gmail.com","addressLine1":"add line 11","addressLine2":"add line 21","city":"San Jose","state":"California","postalCode":"95231","country":"USA"}]}
    
    
Does the contact endpoint handle GET/POST/PUT requests?

    Yes.
    GET -
        get all contacts, URL - http://localhost:8080/mpac/v1/contacts
        get contact by Id - http://localhost:8080/mpac/v1/contacts/{id} [id - contact Id]
    POST - 
        add contact, URL - http://localhost:8080/mpac/v1/addContact
                    Request body - id, name, emailAddress, addressLine1, addressLine2, city, state, postalCode, country.
        Sample Json object - {"id": "222","name": "Kalyan","emailAddress":"kkgorthi@gmail.com","addressLine1":"add line 11","addressLine2": "add line 22","city": "santa clara","state":"Florida","postalCode": "75054","country": "USA"}
    PUT - 
        update contact, URL - http://localhost:8080/mpac/v1/updateContact
                        Request body - id, name, emailAddress, addressLine1, addressLine2, city, state, postalCode, country.
        Sample Json object - {"id": "222","name": "Kalyan","emailAddress":"kkgorthi@gmail.com","addressLine1":"add line 11","addressLine2": "add line 22","city": "santa clara","state":"Florida","postalCode": "75054","country": "USA"}


Can we associate a contact with an account?

    Yes.
    
    
Can we get all contacts for an account?

    Yes.
    GET -
        get all contacts by Account, URL - http://localhost:8080/mpac/v1/accounts/{id}/contacts [id - account Id]
        
        
Have test cases been written?

    Yes.
    run "mvn test" to run all test cases.

